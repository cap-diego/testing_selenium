import json

data = {
	"id": "20d157ba-fdb6-40c6-9a9b-d9a20453c032",
	   "version": "2.0",
		"name": "proyecto testing",
		"url": "http://localhost/accounts/login/",
		"tests": [
			{
				"id": "ef253564-3b67-4fe7-988b-082ed736fe30",
				"name": "hola",
				"commands": [
                {
                    "id": "8f08e351-07a2-4774-95ae-62189b9dd59d",
                    "comment": "",
                    "command": "open",
                    "target": "http://localhost/accounts/login/?next=/sigehos/",
                    "targets": [],
                    "value": ""
                },
                {
                    "id": "c7a1165b-6196-4389-b4fe-6544b157ede5",
                    "comment": "",
                    "command": "setWindowSize",
                    "target": "840x840",
                    "targets": [],
                    "value": ""
                },
                {
                    "id": "e6b45e9c-6757-44dc-b387-1b3a84cf2066",
                    "comment": "",
                    "command": "click",
                    "target": "css=.btn-primary",
                    "targets": [
                        [
                            "css=.btn-primary",
                            "css:finder"
                        ],
                        [
                            "xpath=//button[@type='submit']",
                            "xpath:attributes"
                        ],
                        [
                            "xpath=//form/button",
                            "xpath:position"
                        ],
                        [
                            "xpath=//button[contains(.,'INGRESAR')]",
                            "xpath:innerText"
                        ]
                    ],
                    "value": ""
                }
            ]
			}

		]
	
	}
	

def main():
	import pdb; pdb.set_trace()
	with open('json_file.side', 'w', encoding='utf-8') as f:
		json.dump(data, f, ensure_ascii=False, indent=4)

main()

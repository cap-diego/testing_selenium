

#Utils
import json
from shutil import copyfile, copy2



lista_docs = [
		42253441,
		31973759,
		20334209,
		44483319,
		40946716,
		95300792,
		34379733,
		54303733,
		52460241,
		53465863,
		55870731,
		48803754,
		52769816,
		3748976,
		52642350,
		49862179,
		43784954]

NOMBRE_PROYECTO_TEST = 'proyecto_autom.side'

NOMBRE_TEST = 'cancelar_turno'

COMANDO_BUSCADO = 'asds'

def get_lista_docs():
	return lista_docs[:2]

def main():
	""" Caso empadronamiento con diferentes docs:
	-Seleccionar un tipo de test: otorgar turno clinica medica.
	-Clonar n de ese tipo de test.
	-Cargar una lista de tipos de documento.
	-Modificarlos a c/u con diferentes tipos de documentos.
	-Guardarlos en otros archivo
	"""
	copy2('proyecto_autom.side', 'copia_proyecto_autom.side')
	
	data = None
	with open(NOMBRE_PROYECTO_TEST, 'r+') as f:
		data = json.load(f)

		""" La idea es multiplicar el test de otorgamiento de turno para diferentes pacientes. El flujo seria:
			correr test de otorgar turno para paciente p seguido del test de cancelar turno para paciente p.
		 """
		
		for test in data['tests']:
			if (test['name'] == NOMBRE_TEST):
				test_copiar = test
				print ("Corriendo: {}".format(test['name']))
				print ("	Cantidad de comandos: {}".format((len(test['commands']))))
				for command in test['commands']:
					if('nro_documento' in command['target'] and command['command'] != 'click'):
						command['value'] = '4482133'
						# magia de stack ov
						f.seek(0)        # <--- should reset file position to the beginning.
						json.dump(data, f, indent=4)
						f.truncate()     # remove remaining part
						#fin magia


main()

import subprocess
from abc import ABC
import json
import sys
import os
sys.path.append(os.getcwd())
from utils.constants import *
import requests
from datetime import datetime
import logging

#Si no se importa arriba de todo no funciona
logging.basicConfig(filename='app.log', filemode='a', format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)


class SeleniumObject(ABC):

    def to_dict(self):
        return str(vars(self))

    def __str__(self):
        return str(vars(self))
    
    def __repr__(self):
        return str(vars(self))


def get_new_id():
    import random, string
    cant_digitos_id = [8, 4, 4, 4, 12]
    id = ''
    for cant_digitos in cant_digitos_id:
        x = ''.join(random.choices(string.ascii_lowercase + string.digits, k=cant_digitos))
        id += x + '-'

    return id[:-1]


class ProyectoSelenium(SeleniumObject):

    def __init__(self, id, name, tests=None):
        self.id = id
        self.version = '2.0'
        self.name = name
        self.url = 'http://localhost/accounts/login/?next=/sigehos/'
        if tests is None:
            self.tests = []
        else:
            self.tests = tests
        self.suites = []
        self.suites.append({
            'id': get_new_id(),
            'name': 'Default Suite',
            'persistSession': 'false',
            'parallel': 'false',
            'timeout': 300,
            'tests': []
        }
        )
        """
        [
            {
                'id': 'ID_DEL_PROYECTO',
                'name': 'Default Suite',
                'persistSession': 'false',
                'parallel': 'false',
                'timeout': '300',
                'tests': ['id_test_otorgar_turno']
            }
        ]
        """
        self.urls = []
        self.plugins = []

    def add_test(self, test):
        self.tests.append(test)
        self.suites[0]['tests'].append(test.id)

    def to_side_file(self):
        name = self.name + str('.side')
        with open(PATH_TESTS_OUT + name, 'w', encoding='utf-8') as f:
            cad = self.to_dict()
            cad = cad.replace("'", '"')
            j1 = json.loads(cad)
            json.dump(j1, f, ensure_ascii=True, indent=2)

    def get_estimated_time(self):
        acum = 0
        for test in self.tests:
            for command in test.commands:
                if command.command in COMMAND_WAIT:
                    if not command.target == '':
                        acum += int(command.target)
                else:
                    acum += 200 # Estimo 200msg por comando. 5 comandos = 1 segundo
        return acum/1000


class Test(SeleniumObject):

    def __init__(self, id, name, comandos):
        self.id = id
        self.name = name
        self.commands = comandos

    def add_command(self, command):
        self.commands.append(command)

    def add_pause(self, tiempo):
        self.commands.append(Comando(get_new_id(), 'pause', str(tiempo)))

    def asociar_turno(self, turno):
        if turno.is_valid():
            """
                logica para modificar los comandos del test acorde a las
                caracteristicas del turno
            """
            for co in self.commands:
                command = vars(co)
                if 'nro_documento' in command['target'] and command['command'] != 'click':
                    command['value'] = str(turno.nro_documento)

                if 'select-esp-prof' in command['target'] and command['command'] == 'select':
                    command['value'] = 'label=' + str(turno.esp)

                if 'select-esp-prof' in command['target'] and command['command'] == 'click':
                    command['target'] = command['target'][:len(command['target']) - 2]
                    command['target'] += str(turno.indice_esp + str(')'))

                if 'select-agendas' in command['target'] and command['command'] in COMMAND_SELECT_DATA:
                    command['value'] = 'label=' + turno.label_agenda
                    #command['command'] = 'add selection'
                    command['target'] = 'id=select-agendas'

                if command['command'] == 'click' and ('fc-event:nth-child' in command['target'] and \
                                                      '> .fc-event-inner' in command['target']):
                    partial = 'css=.fc-event:nth-child('
                    final = ') > .fc-event-inner'
                    command['target'] = partial + str(turno.dia) + final

        else:
            raise ValueError

    def asociar_agenda(self, agenda):
        """
            La idea es partir de una plantilla de creación de agenda y poder generar un
            proyecto de selenium que permita ser corrido.
        """
        if agenda.is_valid():

            for co in self.commands:

                co = vars(co)
                # Setear nombre agenda
                if co['command'] in COMMAND_INPUT_DATA and 'nomagenda' in co['target']:
                    co['value'] = agenda.nombre_agenda

                #Setear especialidades (por ahora solo una)
                if co['command'] in COMMAND_INPUT_DATA and 'id=inputFilter' == co['target'] and \
                        co['value'] not in COMMAND_SEND_DATA and co['value'] not in COMMAND_INPUT_DATA:
                    co['value'] = agenda.especialidades[0]

                #Setear profs (por ahora solo uno)

                if co['command'] in COMMAND_INPUT_DATA and 'css=#autoprof #inputFilter' == co['target'] and \
                    co['value'] not in COMMAND_SEND_DATA:
                    #Cambio el prof
                    co['value'] = agenda.profesionales[0]
                if co['command'] in COMMAND_REP:
                    co['target'] = '5'

                if co['command'] in COMMAND_SELECT_DATA and co['target'].replace('id=', '') in COMMAND_WEEK_DAYS:
                    #Es un comando de elegir dia
                    if len(agenda.dias) > 0:
                        #Si la agenda sigue tenendo dias que asignar
                        if co['target'].replace('id=', '') != agenda.dias[0]:
                            #Si el dia del comando no es un dia de nuestros dias de agenda lo cambio
                            co['target'] = 'id=' + str(COMMAND_WEEK_DAYS[agenda.dias[0]-1])
                        #Elimino el dia de la agenda para no volver a contarlo
                        del(agenda.dias[0])
                    else:
                        #Si ya no tengo mas dias elimino el comando
                        #Eliminar esta fallando asi que 'borro' el comando
                        co['command'] = 'pause'
                        co['target'] = ''

                if co['command'] in COMMAND_INPUT_DATA and 'css=#horadesde' in co['target'] and \
                        co['value'] not in COMMAND_SEND_DATA:
                    co['value'] = str(agenda.hs_in)

                if co['command'] in COMMAND_INPUT_DATA and 'css=#horahasta' in co['target'] and \
                        co['value'] not in COMMAND_SEND_DATA:
                    co['value'] = str(agenda.hs_fin)

                if co['command'] in COMMAND_SELECT_DATA	and 'cantturnosSelect' in co['target']:
                    co['value'] = 'index=' + str(agenda.cantidad_turnos-1)

        else:
            raise ValueError

    def asociar_cancelar_turno(self, ct):
        if ct.is_valid():
            for co in self.commands:
                co = vars(co)

                if co['command'] in COMMAND_INPUT_DATA and 'nro_documento' in co['target']:
                    co['value'] = ct.nro_documento

                if co['command'] in COMMAND_SELECT_DATA and co['value'] == '' and \
                        'css=tr:nth-child' in co['target'] and '.cancelar-turno' in co['target']:
                    partial = 'css=tr:nth-child('
                    final = ') .cancelar-turno'
                    co['target'] = partial + str(ct.indice) + final
        else:
            raise ValueError

    def add_sign_out(self):
        out = Comando(get_new_id(), 'open', 'http://localhost/accounts/logout/', value='', targets=[], comment='')
        self.add_command(out)


class Turno(object):

    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.nro_documento = None
        self.indice_agenda = None
        self.indice_prof = None
        self.esp = None
        self.indice_esp = None
        self.prof = None
        self.dia = None
        self.nombre_agenda = None
        self.label_agenda = None

    def set_doc_paciente(self, doc):
        self.nro_documento = doc

    def set_nomb_paciente(self, nomb, ap):
        self.nombre = nomb
        self.apellido = ap

    def set_nombre_agenda(self, agenda):
        self.nombre_agenda = agenda

    def set_indice_agenda(self, indice):
        self.indice_agenda = indice

    def set_especialidad(self, esp, indice):
        self.esp = esp
        self.indice_esp = indice

    def set_profesional(self, prof, indice):
        self.prof = prof
        self.indice_prof = indice

    def set_dia_turno(self, indice):
        self.dia = indice

    def is_valid(self):
        return ((self.nro_documento is not None) or (self.nombre != '' and self.apellido != '')) and \
        self.indice_agenda is not None and self.dia is not None and \
        ((self.esp is not None and self.indice_esp is not None) or \
        (self.prof is not None and self.indice_prof is not None))


class Agenda(object):
    """ Una agenda puede tener asociado una lista de nombres profesionales y de especialidades.
        Puede estar agregada a un varios grupos de agendas.
        Tiene una lista de dias (lunes=1, domingo=7) para los cuales funciona.
        Tiene una tupla (hora_in, hora_fin).
        Tiene una duracion de turnos.
    """
    def __init__(self, na='', esp=None, prof=None, dias=None, hs_in=None, hs_fin=None, cantidad_turnos=None, grupo_agendas_agregar=None):
        self.nombre_agenda = na
        if prof is None:
            self.profesionales = []
        else:
            self.profesionales = prof
        if esp is None:
            self.especialidades = []
        else:
            self.especialidades = esp
        if dias is None:
            self.dias = []
        else:
            self.dias = dias

        self.hs_in = hs_in
        self.hs_fin = hs_fin
        self.grupo_agendas_agregada = grupo_agendas_agregar
        self.cantidad_turnos = cantidad_turnos

    def __repr__(self):
        return str(vars(self))

    def is_valid(self):
        return self.nombre_agenda != '' and len(self.especialidades) > 0 and \
            len(self.profesionales) > 0 and self.hs_in is not None and \
            self.hs_fin is not None and self.cantidad_turnos > 0


class CancelarTurno(object):

    def __repr__(self):
        return str(vars(self))

    def __init__(self, indice=None, doc=None):
        self.nro_documento = str(doc)
        if indice is not None:
            self.indice = str(indice)
        else:
            self.indice = None

    def is_valid(self):
        return self.nro_documento is not None and self.indice is not None


def change_work_dir():
    # Check if New path exists
    import os
    if 'src' in os.getcwd():
        nuevo_path = os.getcwd().replace('/src', '')
        if os.path.exists(nuevo_path):
            os.chdir(nuevo_path)


def crear_turno(agenda, doc, n_turno, esp):
    turno = Turno()
    turno.set_doc_paciente(doc) # 132371510
    turno.set_dia_turno(n_turno)
    turno.set_especialidad(esp, ESPECIALIDADES[esp])
    turno.set_indice_agenda('13')
    label_agenda = str(agenda['id']) + ' - ' + str(agenda['nomagenda'])
    turno.label_agenda = label_agenda
    turno.set_nombre_agenda(agenda['nomagenda'])
    return turno


def crear_agenda(nombre, esp_shortcut, prof_shorcut, array_dias, inicio, fin, cant_turnos):
    # Agenda(nombre, especiaidad_shorcut, prof_shorcut, dias_disp, horainicio, horafin, cant_turnos_diarios, agenda_grupo)
    agenda = Agenda(nombre, [esp_shortcut], [prof_shorcut], array_dias, inicio, fin, cant_turnos, 'todas')
    return agenda



def cancelar_turno(indice, nro_doc_pac):
    ct = CancelarTurno(nro_doc_pac, indice)
    return ct


class Comando(SeleniumObject):

    def __init__(self, id='', command='', target='', value='', targets=[], comment=''):
        self.id = id
        self.comment = comment
        self.command = command
        self.target = target
        self.targets = targets
        self.value = value


def conseguir_agenda(agenda, funcion_de_get_token, efector_id):

    url = 'http://localhost/sigehos/lgc_common/api/v2/agendas'
    params = {
        'nombre': agenda.nombre_agenda,
        'efector_id': efector_id
    }
    header = {
        'Content-Type': 'application/json',
        'Authorization': funcion_de_get_token
    }
    try:
        response = requests.get(url, params=params, headers=header, timeout=TIMEOUT)
        #print(url + "/?nombres=" + agenda.nombre_agenda)
    except requests.exceptions.RequestException as e:
        logging.error('Error al consultar por la agenda {} en efector {} '.format(agenda.nombre_agenda,\
                                                                efector_id), exc_info=True)
        raise e + str('Error al consultar agendas')

    if response.status_code != STATUS_HTTP_200:
        logging.error('Respuesta {} al consultar por la agenda {} en efector {}'.format(response.status_code, agenda.nombre_agenda, \
                                                                                  efector_id))

        raise EnvironmentError('Respuesta {} al consultar por la agenda {} en efector {}'.format(response.status_code, agenda.nombre_agenda, \
                                                                                  efector_id))
    if response.json()['count'] == 0:
        logging.error("No se encontro la agenda {} en efector {}".format(agenda.nombre_agenda, efector_id))

        raise EnvironmentError("No se encontro la agenda {} en efector {}".format(agenda.nombre_agenda, efector_id))

    agendas = response.json()['results']
    current_max_id = 0
    current_agenda = None
    for ag in agendas:
        if ag['id'] > current_max_id:
            current_max_id = ag['id']
            current_agenda = ag
    return current_agenda

def conseguir_token():
    body = {
        'username': 'administrador',
        'password': 'clavefacil',
        'client_id': '2c947d53e5a857e1805f',
        'client_secret': '6b46cb5fe78a9d811e8b2b93b95d2562d3a4fc40',
        'grant_type': 'password'
    }
    header = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    try:
        response = requests.post('http://localhost/oauth2/access_token/', data=body, headers=header, timeout=TIMEOUT)
        
    except requests.exceptions.RequestException as e:
        logging.error('Error al consultar token.', exc_info=True)
        raise e + str("Error al conseguir el token de acceso")

    if response.status_code != STATUS_HTTP_200:
        logging.error('Error al conseguir token de acceso')
        raise EnvironmentError('Error al conseguir token de acceso')

    response_json = response.json()
    token = 'Bearer ' + response_json['access_token']
    return token


def conseguir_datos_paciente(turno, get_token, efector_id):
    url = 'http://localhost/sigehos/lgc_common/api/v2/personas/pacientes/?'
    params = {
        'nro_documento': turno.nro_documento,
        'nombres': turno.nombre,
        'apellido': turno.apellido,
        'efector_id': efector_id
    }
    header = {
        'Content-Type': 'application/json',
        'Authorization': get_token
    }
    try:
        response = requests.get(url, params=params, headers=header, timeout=TIMEOUT)
    except requests.exceptions.RequestException as e:
        logging.error('Error al consultar paciente {} {} {} en efector {}, con el turno de agenda {} \
        prof: {} esp: {}'.format(turno.nro_documento, turno.nombre, turno.apellido, efector_id,\
                                    turno.nombre_agenda, turno.prof, turno.esp), exc_info=True)
        raise e + str('Error al consultar pacientes')

    if response.status_code != STATUS_HTTP_200:
        logging.error('Respuesta {} al consultar por paciente {} en efector {}'.format(response.status_code, turno.nro_documento , efector_id))

        raise EnvironmentError('Respuesta {} al consultar por paciente {} en efector {}'.format(response.status_code, turno.nro_documento, \
                                                                                  efector_id))

    resp = response.json()

    if resp['count'] == 0:
        logging.error('No se encontro al paciente {} {} {} en efector {}'.format(turno.nro_documento, \
                                                            turno.nombre, turno.apellido, efector_id))
        raise ValueError("No se encontro al paciente")
    elif resp['count'] > 1:
        logging.error('Se encontraron mas de un resultados al buscar al paciente {} {} {} en efector {}'.format(turno.nro_documento, \
                                                            turno.nombre, turno.apellido, efector_id))
        raise ValueError("Error: se encontraron mas de un pacientes")
    return resp['results'][0]

def conseguir_turno_en_sigehos(turno_sel, get_token, paciente, efector_id):
    """A partir del turno otorgado con selenium lo buscamos en el endpoint de turnos de sigehos para obtener datos
    que nos permitan encontrarlo en gestion de turnos"""
    url = 'http://localhost/sigehos/lgc_common/api/v2/turnos/?'
    params = {
        'efector_id': efector_id,
        'mostrar_otorgados': 'true',
        'mostrar_cancelados': 'false',
        'mostrar_confirmados': 'true',
        'persona': paciente['id'],
        'horario_turno__gt': datetime.today().strftime('%Y-%m-%d') + 'T00:00:00'
    }
    header = {
        'Content-Type': 'application/json',
        'Authorization': get_token
    }
    try:
        response = requests.get(url, params=params, headers=header, timeout=TIMEOUT)
    except requests.exceptions.RequestException as e:
        logging.error("Error al consultar turno de paciente {} en efector {}".format(turno_sel.nro_documento, efector_id))
        raise e + str('Error al consultar turno')

    if response.status_code != STATUS_HTTP_200:
        logging.error('Respuesta {} al consultar por turno de paciente {} en efector {}'.format(response.status_code, turno_sel.nro_documento, efector_id))

        raise EnvironmentError('Respuesta {} al consultar turno de paciente {} en efector {}'.format(response.status_code, turno_sel.nro_documento, \
                                                                                  efector_id))

    resp = response.json()
    if resp['count'] == 0:
        print('error')
        #raise ValueError("No se encontraron turnos para el paciente")

    if resp['count'] > 1:
        contador = 1
        for turno in resp['results']:
            if turno_sel.nombre_agenda == turno['agenda__nombre'] and (int(turno['persona__nro_doc']) == int(turno_sel.nro_documento) or \
                                                                       (turno_sel.apellido == turno['persona__apellido'])):
                return contador
            contador += 1
    else:
        return 1


def armar_y_retornar_test(filein, test):
    try:
        with open(filein, 'r') as f2:
            plantilla = json.load(f2)
    except FileNotFoundError as e:
        error = "Error al abrir plantilla"
        logging.error(error, exc_info=True)
        raise e

    for c in plantilla['commands']:#turno['commands']:
        c2 = Comando(get_new_id(), c['command'], c['target'], c['value'], [])
        test.add_command(c2)
        #test.add_pause(500)
    #test.add_sign_out()

    return test


def correr_test(command, p1):
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    try:
        output, error = process.communicate()
    except:
        logging.error("Error al correr el comando {}".format(command))

    logging.info("SE CORRIO EL PROYECTO: {} y el output fue {}".format(p1.name, output))


def main():
    change_work_dir()
    logging.info(datetime.today())
    if len(sys.argv) > 1:
        if str(sys.argv[1]) != 'ag':
            if str(sys.argv[1]) != 'tur':
                if str(sys.argv[1]) != 'ct':
                    print('Error arg')
                    return 
                else:
                    ejec = 'ct'
            else:
                ejec = 'tur'
        else:
            ejec = 'ag'
    else:
        print("Faltan args")
        return
    """
        El flujo es: dada una plantilla de un tipo de test leerla y parsearla a un dict
        con json.load. Con este informacion podemos crear nuestro proyecto de selenium,
        y crear un test t1.
        Recorrer los comandos del test de la plantilla y crearselos a t1.
        Crear una agenda o turno y asociarla con el test al final.
        Lo ultimo es agregar los tests al proyecto y usar p.to_side_file() para
        obtener el archivo a levantar con selenium
    """

    p1 = ProyectoSelenium(get_new_id(), 'proyecto_agendas')

    especialidades = []
    agendas_creadas = []
    for especialidad in ESPECIALIDADES.keys():
        especialidades.append(especialidad)
        test_crear_agenda = Test(get_new_id(), 'test_agenda/'+str(especialidad), [])

        agenda_personalizadas = crear_agenda('1_agenda/'+str(especialidad.replace(" ", '')), especialidad, 'FLAV', [4, 5, 6, 7], 11, 15, 4)
        agendas_creadas.append(agenda_personalizadas)
        test_crear_agenda = armar_y_retornar_test(PLANTILLA_CREAR_AGENDA, test_crear_agenda)

        test_crear_agenda.asociar_agenda(agenda_personalizadas)

        p1.add_test(test_crear_agenda)

    if(ejec=='ag'):
        p1.to_side_file()

    print("Finalizado el armado de test de agendas. Tiempo estimado: {} segundos".format(p1.get_estimated_time()))
    logging.info("Finalizado el armado de test de agendas. Tiempo estimado: {} segundos".format(p1.get_estimated_time()))

    p5 = ProyectoSelenium(get_new_id(), 'proyecto_turnos')
    p6 = ProyectoSelenium(get_new_id(), 'proyecto_cancelar_turno')
    i = 0

    for agenda in agendas_creadas:
        agenda_sigehos = conseguir_agenda(agenda, conseguir_token(), 55)# Con esto consigo el label de la agenda
        test_otorgar_turno = Test(get_new_id(), 'test_turno/{}'.format(especialidades[i]), [])
        test_otorgar_turno_no_disp = Test(get_new_id(), 'test_turno_no_disp/{}'.format(especialidades[i]), [])
        test_cancelar_turno = Test(get_new_id(), 'cancelar_turno/{}'.format(especialidades[i]), [])

        turno_personalizado_a_otorgar_no_disp = crear_turno(agenda_sigehos, PACIENTES[i], 11, especialidades[i])
        turno_personalizado_a_otorgar = crear_turno(agenda_sigehos, PACIENTES[i], 11, especialidades[i])

        indice_turno = conseguir_turno_en_sigehos(turno_personalizado_a_otorgar, conseguir_token(), conseguir_datos_paciente(turno_personalizado_a_otorgar, conseguir_token(), 55), 55)
        test_personalizado_cancelar_turno = CancelarTurno(indice_turno, PACIENTES[i])

        test_otorgar_turno = armar_y_retornar_test(PLANTILLA_OTORGAR_TURNO, test_otorgar_turno)
        test_otorgar_turno_no_disp = armar_y_retornar_test(PLANTILLA_OTORGAR_TURNO_NO_DISPONIBLE, test_otorgar_turno_no_disp)
        test_cancelar_turno = armar_y_retornar_test(PLANTILLA_CANCELAR_TURNO, test_cancelar_turno)

        test_otorgar_turno.asociar_turno(turno_personalizado_a_otorgar)
        test_otorgar_turno_no_disp.asociar_turno(turno_personalizado_a_otorgar_no_disp)
        test_cancelar_turno.asociar_cancelar_turno(test_personalizado_cancelar_turno)

        p5.add_test(test_otorgar_turno)
        p5.add_test(test_otorgar_turno_no_disp)
        p6.add_test(test_cancelar_turno)
        i += 1

    if ejec == 'ct':
        p6.to_side_file()
    if ejec == 'tur':
        p5.to_side_file()

    print("Finalizado el armado de test de turnos. Tiempo estimado: {} segundos".format(p5.get_estimated_time()))
    logging.info("Finalizado el armado de test de turnos. Tiempo estimado: {} segundos".format(p5.get_estimated_time()))

    return 0


main()


"""
test_cancelar_turno = Test('id_test_cancelar_turno', 'cancelar turno', [])
indice_turno = conseguir_turno_en_sigehos(turno_personalizado_a_otorgar, conseguir_token(), conseguir_datos_paciente(turno_personalizado_a_otorgar, conseguir_token(), 55), 55)
ct = cancelar_turno(indice_turno)
test_cancelar_turno = armar_y_retornar_test(PLANTILLA_CANCELAR_TURNO, test_cancelar_turno)
test_cancelar_turno.asociar_cancelar_turno(ct)
"""

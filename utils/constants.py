
# Rutas y archivos
CREAR_AGENDA = 'crear_agenda_sin_login.side'
OTORGAR_TURNO = 'otorgar_turno.side'
OTORGAR_TURNO_NO_DISPONIBLE = 'otorgar_turno_no_disponible.side'
CANCELAR_TURNO = 'cancelar_turno.side'
PATH_PLANTILLAS = 'plantillas/'
NOMBRE_PLANTILLA_LOGIN = 'login.side'
NOMBRE_PLANTILLA_SELENIUM = 'proyecto_autom.side'

PATH_FILES_IN = 'tests_in/'
PATH_TESTS_OUT = 'tests_out/'


URL_TOKEN = 'http://oauth2/access_token/'
TIMEOUT = 30
STATUS_HTTP_200 = 200

PLANTILLA_CANCELAR_TURNO = PATH_PLANTILLAS + CANCELAR_TURNO
PLANTILLA_OTORGAR_TURNO = PATH_PLANTILLAS + OTORGAR_TURNO
PLANTILLA_CREAR_AGENDA = PATH_PLANTILLAS + CREAR_AGENDA
PLANTILLA_SELENIUM = PATH_FILES_IN + NOMBRE_PLANTILLA_SELENIUM
PLANTILLA_LOGIN = PATH_PLANTILLAS + NOMBRE_PLANTILLA_LOGIN
PLANTILLA_OTORGAR_TURNO_NO_DISPONIBLE = PATH_PLANTILLAS + OTORGAR_TURNO_NO_DISPONIBLE

# Comandos
COMMAND_INPUT_DATA = ['send keys', 'type', 'sendKeys']
COMMAND_SEND_DATA = ['KEY_DOWN', 'KEY_ENTER', '${KEY_DOWN}', '${KEY_ENTER}', 'KEY_BACKSPACE', '${KEY_BACKSPACE}']
COMMAND_REP = ['times']
COMMAND_WAIT = ['pause']
COMMAND_WEEK_DAYS = ['lun', 'mar', 'mie', 'jue', 'vie', 'sab', 'dom']
COMMAND_SELECT_DATA = ['click', 'select', 'add selection']


ESPECIALIDADES = {
        'CLINICA MEDICA': '1',
        'CONTROL DE EMBARAZO': '2',
        'ECOGRAFIA': '3',
        'FONOAUDIOLOGIA': '4',
        'KINESIOLOGIA': '5',
        'LABORATORIO': '6',
        'NUTRICION': '7',
        'OBSTETRICIA': '8',
        'ODONTOLOGIA': '9',
        'PEDIATRIA': '10',
        'PSICOLOGIA': '11',
        'PSICOPEDAGOGIA': '12',
        'TOCOGINECOLOGIA': '13',
        'TRABAJO SOCIAL': '14'
}

# Esto es para pruebas.
PACIENTES = [
        52455884,
        54294016,
        46120843,
        94944066,
        94985642,
        94985634,
        43000304,
        55602457,
        94985641,
        55066013,
        53413939,
        41009855,
        13052259,
        53096836,
        44463786
]
